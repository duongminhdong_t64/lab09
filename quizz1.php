<!DOCTYPE html>
<!--code by webdevtrick (webdevtrick.com) -->

<head>
    <meta charset=UTF-8" />


    <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
    <div id="page-wrap">

        <h1>Trang 1 : Câu hỏi từ 1 đến 5</h1>

        <form action="quizz2.php" method="post" id="quiz">

            <ol>

                <li>

                    <h3>Câu 1: Con gì có bốn chân kêu meo meo</h3>

                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-A" value="A" />
                        <label for="question-1-answers-A">A) Con chó </label>
                    </div>

                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-B" value="B" />
                        <label for="question-1-answers-B">B) Con Mèo</label>
                    </div>

                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-C" value="C" />
                        <label for="question-1-answers-C">C) Con lợn</label>
                    </div>

                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-D" value="D" />
                        <label for="question-1-answers-D">D) Con bò</label>
                    </div>

                </li>

                <li>

                    <h3>Câu 2: Trong nhà có bốn người Hoa yêu ai nhất</h3>

                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-A" value="A" />
                        <label for="question-2-answers-A">A) Ông hàng xóm</label>
                    </div>

                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-B" value="B" />
                        <label for="question-2-answers-B">B) Chú bảo vệ </label>
                    </div>

                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-C" value="C" />
                        <label for="question-2-answers-C">C) Anh bộ đội</label>
                    </div>

                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-D" value="D" />
                        <label for="question-2-answers-D">D) Bà nội</label>
                    </div>

                </li>

                <li>

                    <h3>Câu 3: "Hạt gạo làng ta" là ca khúc do ai sáng tác</h3>

                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-A" value="A" />
                        <label for="question-3-answers-A">A) Mạc Đĩnh Chi</label>
                    </div>

                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-B" value="B" />
                        <label for="question-3-answers-B">B) Tố Hữu </label>
                    </div>

                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-C" value="C" />
                        <label for="question-3-answers-C">C) Trần Đăng Khoa </label>
                    </div>

                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-D" value="D" />
                        <label for="question-3-answers-D">D) Hàn Mạc Tử </label>
                    </div>

                </li>

                <li>

                    <h3>Câu 4: TotanHam là đội bóng của nước nào ?</h3>

                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-A" value="A" />
                        <label for="question-4-answers-A">A) Pháp </label>
                    </div>

                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-B" value="B" />
                        <label for="question-4-answers-B">B) Ý</label>
                    </div>

                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-C" value="C" />
                        <label for="question-4-answers-C">C) Nhật</label>
                    </div>

                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-D" value="D" />
                        <label for="question-4-answers-D">D) Pakistan </label>
                    </div>

                </li>

                <li>

                    <h3>Câu 5: Chiến tranh giữa Nga và quốc gia nào đang xảy ra hiện nay ?</h3>

                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-A" value="A" />
                        <label for="question-5-answers-A">A) Mỹ </label>
                    </div>

                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-B" value="B" />
                        <label for="question-5-answers-B">B) Nhật </label>
                    </div>

                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-C" value="C" />
                        <label for="question-5-answers-C">C) Anh </label>
                    </div>

                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-D" value="D" />
                        <label for="question-5-answers-D">D) Ukraina </label>
                    </div>

                </li>

            </ol>

            <input type="submit" value="Next" class="submitbtn" />

        </form>

    </div>


</body>

</html>