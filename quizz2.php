<!DOCTYPE html>
<!--code by webdevtrick (webdevtrick.com) -->

<head>
    <meta charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
    <?php
    session_start();
    $_SESSION["question-1-answers"] = $_POST['question-1-answers'] ?? "";
    $_SESSION["question-2-answers"] = $_POST['question-2-answers'] ?? "";
    $_SESSION["question-3-answers"] = $_POST['question-3-answers'] ?? "";
    $_SESSION["question-4-answers"] = $_POST['question-4-answers'] ?? "";
    $_SESSION["question-5-answers"] = $_POST['question-5-answers'] ?? "";

    ?>
    <div id="page-wrap">

        <h1> Trang 2 Câu hỏi từ 6 đến 10 </h1>

        <form action="result.php" method="post" id="quiz">

            <ol>

                <li>

                    <h3>Câu 6: Bình phương 2 cạnh góc vuông bằng cạnh huyền trong tam giác vuông là định lý nào sau đây ?</h3>

                    <div>
                        <input type="radio" name="question-6-answers" id="question-6-answers-A" value="A" />
                        <label for="question-6-answers-A">A) Định lý Tallet </label>
                    </div>

                    <div>
                        <input type="radio" name="question-6-answers" id="question-6-answers-B" value="B" />
                        <label for="question-6-answers-B">B) Định lý Độ lệch chuẩn </label>
                    </div>

                    <div>
                        <input type="radio" name="question-6-answers" id="question-6-answers-C" value="C" />
                        <label for="question-6-answers-C">C) Định lý Pytago </label>
                    </div>

                    <div>
                        <input type="radio" name="question-6-answers" id="question-6-answers-D" value="D" />
                        <label for="question-6-answers-D">D) Định lý Newton </label>
                    </div>

                </li>

                <li>

                    <h3>Câu 7: "Đoàn quân việt nam đi chung lòng cứu quốc ..."" là lời của bài hát nào sau đây</h3>

                    <div>
                        <input type="radio" name="question-7-answers" id="question-7-answers-A" value="A" />
                        <label for="question-7-answers-A">A) Hà Nội của tôi </label>
                    </div>

                    <div>
                        <input type="radio" name="question-7-answers" id="question-7-answers-B" value="B" />
                        <label for="question-7-answers-B">B) thành phố Hồ Chí Minh muôn năm </label>
                    </div>

                    <div>
                        <input type="radio" name="question-7-answers" id="question-7-answers-C" value="C" />
                        <label for="question-7-answers-C">C) Sức mạnh tình yêu nước </label>
                    </div>

                    <div>
                        <input type="radio" name="question-7-answers" id="question-7-answers-D" value="D" />
                        <label for="question-7-answers-D">D) Tiến quân ca </label>
                    </div>

                </li>

                <li>

                    <h3>Câu 8: Điền vào chỗ trống "Đi một ngày đàng học ... sàng khôn" </h3>

                    <div>
                        <input type="radio" name="question-8-answers" id="question-8-answers-A" value="A" />
                        <label for="question-8-answers-A">A) mười</label>
                    </div>

                    <div>
                        <input type="radio" name="question-8-answers" id="question-8-answers-B" value="B" />
                        <label for="question-8-answers-B">B) rổ </label>
                    </div>

                    <div>
                        <input type="radio" name="question-8-answers" id="question-8-answers-C" value="C" />
                        <label for="question-8-answers-C">C) một </label>
                    </div>

                    <div>
                        <input type="radio" name="question-8-answers" id="question-8-answers-D" value="D" />
                        <label for="question-8-answers-D">D) ít </label>
                    </div>

                </li>

                <li>

                    <h3>Câu 9: Khi không ăn một thời gian dài ta sẽ cảm thấy gì ?</h3>

                    <div>
                        <input type="radio" name="question-9-answers" id="question-9-answers-A" value="A" />
                        <label for="question-9-answers-A">A) Khát Nước </label>
                    </div>

                    <div>
                        <input type="radio" name="question-9-answers" id="question-9-answers-B" value="B" />
                        <label for="question-9-answers-B">B) Buồn Ngủ </label>
                    </div>

                    <div>
                        <input type="radio" name="question-9-answers" id="question-9-answers-C" value="C" />
                        <label for="question-9-answers-C">C) Đói </label>
                    </div>

                    <div>
                        <input type="radio" name="question-9-answers" id="question-9-answers-D" value="D" />
                        <label for="question-9-answers-D">D) Vui Vẻ </label>
                    </div>

                </li>

                <li>

                    <h3>Câu 10: Lịch nào dài nhất thế giới ? </h3>

                    <div>
                        <input type="radio" name="question-10-answers" id="question-10-answers-A" value="A" />
                        <label for="question-10-answers-A">A) Lịch Maya </label>
                    </div>

                    <div>
                        <input type="radio" name="question-10-answers" id="question-10-answers-B" value="B" />
                        <label for="question-10-answers-B">B) Lịch Sử </label>
                    </div>

                    <div>
                        <input type="radio" name="question-10-answers" id="question-10-answers-C" value="C" />
                        <label for="question-10-answers-C">C) Lịch Làm Việc </label>
                    </div>

                    <div>
                        <input type="radio" name="question-10-answers" id="question-10-answers-D" value="D" />
                        <label for="question-10-answers-D">D) Lịch hẹn hò </label>
                    </div>

                </li>

            </ol>

            <input type="submit" value="Nộp Bài" class="submitbtn" />

        </form>

    </div>


</body>

</html>