<!DOCTYPE html>
<!--code by webdevtrick (webdevtrick.com) -->
<html>

<head>
    <meta charset=UTF-8" />

    <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>

    <div id="page-wrap">

        <h1>Kết Quả bạn đạt được :</h1>

        <?php
        // Mảng kết quả 
        $answers = array('B', 'D', 'C', 'A', 'D', 'C', 'D', 'C', 'C', 'B');
        // Mảng kết quả lựa chọn
        $answerSelect = array();
        session_start();
        $answerSelect[] = $_SESSION['question-1-answers'] ?? "";
        $answerSelect[] = $_SESSION['question-2-answers'] ?? "";
        $answerSelect[] = $_SESSION['question-3-answers'] ?? "";
        $answerSelect[] = $_SESSION['question-4-answers'] ?? "";
        $answerSelect[] = $_SESSION['question-5-answers'] ?? "";
        $answerSelect[] = $_POST['question-6-answers'] ?? "";
        $answerSelect[] = $_POST['question-7-answers'] ?? "";
        $answerSelect[] = $_POST['question-8-answers'] ?? "";
        $answerSelect[] = $_POST['question-9-answers'] ?? "";
        $answerSelect[] = $_POST['question-10-answers'] ?? "";

        $totalCorrect = 0;
        $comment = "";

        for ($i = 0; $i < count($answers); $i++) {
            if ($answerSelect[$i] == $answers[$i]) {
                $totalCorrect++;
            }
        }

        echo "<div id='results'>$totalCorrect / 10 câu đúng</div>";
        if ($totalCorrect < 4) {
            echo "<div id='results'>Bạn quá kém, bạn cần ôn tập thêm !</div>";
        } elseif ($totalCorrect >= 4 && $totalCorrect <= 7) {
            echo "<div id='results'> Cũng bình thường thôi hehe !  </div>";
        } else {
            echo "<div id='results'> Sắp sửa làm được trợ giảng lớp PHP rồi đó !! </div>";
        }

        ?>

    </div>

</body>

</html>